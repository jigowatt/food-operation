import javax.swing.*;

public class FoodOrderMachine {
    private JPanel root;
    private JButton tempuraButton;
    private JButton ramenButton;
    private JButton udonButton;
    private JButton karaageButton;
    private JButton gyozaButton;
    private JButton yakisobaButton;
    private JTextPane receivedInfo;
    private JButton check;
    private JLabel TotalInfo;
    private JLabel price;
    private JLabel OrderedItems;
    int total = 0;

    void order(String food) {
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order" + food + "?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);

        if (confirmation == 0) {
            receivedInfo.setText(receivedInfo.getText() + food + "\n");

            JOptionPane.showMessageDialog(null, "Order for " + food + " received.");

            total += 100;
            price.setText(total + " yen");
        }
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodOrderMachine");
        frame.setContentPane(new FoodOrderMachine().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    public FoodOrderMachine() {

        check.addActionListener(e -> {
            int confirmation = JOptionPane.showConfirmDialog(null,
                    "Would you like to checkout?",
                    "Checkout Confirmation",
                    JOptionPane.YES_NO_OPTION);

            if (confirmation == 0) {
                JOptionPane.showMessageDialog(null, "Thank you. The price is " + total + " yen.");
                receivedInfo.setText("");
                total = 0;
                price.setText(total + " yen");
            }

        });

        tempuraButton.addActionListener(e -> order("Tempura"));
        karaageButton.addActionListener(e -> order("Karaage"));

        gyozaButton.addActionListener(e -> order("gyoza"));
        udonButton.addActionListener(e -> order("udon"));
        ramenButton.addActionListener(e -> order("ramen"));
        yakisobaButton.addActionListener(e -> order("yakisoba"));

    }
}

import javax.swing.*;

public class ver2_FoodOrderMachine {
    private JPanel root;
    private JLabel topLabel;
    private JButton tempuraButton;
    private JButton ramenButton;
    private JButton udonButton;
    private JButton karaageButton;
    private JButton gyozaButton;
    private JButton yakisobaButton;
    private JLabel OrderedItems;
    private JTextPane receivedInfo;
    private JLabel TotalInfo;
    private JButton cheackOutButton;
    private JLabel TotalPrice;
    int total = 0;

    void order(String food, int price) {
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order" + food + "?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);

        if (confirmation == 0) {
            receivedInfo.setText(receivedInfo.getText() + food + " " + price + "yen\n");
            JOptionPane.showMessageDialog(null, "Order for " + food + " received.");

            total += price;
            TotalPrice.setText(total + " yen");
        }
    }

    public ver2_FoodOrderMachine() {
        tempuraButton.addActionListener(e -> order("Tempura", 880));

        tempuraButton.setIcon(new ImageIcon(
           this.getClass().getResource("foodmenu/tempura.jpg")
        ));

        ramenButton.addActionListener(e -> order("Ramen", 720));

        ramenButton.setIcon(new ImageIcon(
                this.getClass().getResource("foodmenu/ramen.jpg")
        ));

        udonButton.addActionListener(e -> order("Udon", 320));

        udonButton.setIcon(new ImageIcon(
                this.getClass().getResource("foodmenu/udon.jpg")
        ));

        karaageButton.addActionListener(e -> order("Udon", 320));

        karaageButton.setIcon(new ImageIcon(
                this.getClass().getResource("foodmenu/karaage.jpg")
        ));

        gyozaButton.addActionListener(e -> order("Gyoza", 220));

        gyozaButton.setIcon(new ImageIcon(
                this.getClass().getResource("foodmenu/gyoza.jpg")
        ));

        yakisobaButton.addActionListener(e -> order("yakisoba", 440));

        yakisobaButton.setIcon(new ImageIcon(
                this.getClass().getResource("foodmenu/yakisoba.jpg")
        ));

        cheackOutButton.addActionListener(e -> {
            int confirmation = JOptionPane.showConfirmDialog(null,
                    "Would you like to checkout?",
                    "Checkout Confirmation",
                    JOptionPane.YES_NO_OPTION);

            if (confirmation == 0) {
                JOptionPane.showMessageDialog(null, "Thank you. The price is " + total + " yen.");
                receivedInfo.setText("");
                total = 0;
                TotalPrice.setText(total + " yen");
            }

        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("ver2_FoodOrderMachine");
        frame.setContentPane(new ver2_FoodOrderMachine().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

}

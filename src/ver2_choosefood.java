import java.util.Scanner;

public class ver2_choosefood {
    static void order(String food){
        System.out.println("You have ordered " + food + ". Thank you!");
    }


    public static void main(String[] args) {
        System.out.println("Would you like to order:");
        System.out.println("1. Tempura");
        System.out.println("2. Ramen");
        System.out.println("1. Udon");
        Scanner userIn = new Scanner(System.in);
        System.out.print("Your order [1-3]:");
        String foodNumber = userIn.nextLine();
        userIn.close();
        switch (foodNumber) {
            case "1":
                order("Tempura");
                break;

            case "2":
                order("Ramen");
                break;

            case "3":
                order("Udon");
                break;

        }
    }
}
